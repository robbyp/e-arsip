<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//json
Route::post('/submit_login','LoginController@submitLogin')->name('loginwoy');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/data/akreditasi/json', 'AkreditasiController@dataTable')->name('table.akreditasi');
Route::get('/data/dokumentasi/json', 'DokumentasiController@dataTable')->name('table.dokumentasi');
Route::get('/data/kemahasiswaan/json', 'KemahasiswaanController@dataTable')->name('table.kemahasiswaan');
Route::get('/data/kegiatan/json', 'KegiatanController@dataTable')->name('table.kegiatan');
Route::get('/data/akreditasi/json/{id_prodi}', 'AkreditasiController@dataTable');
Route::get('/data/kegiatan/json/{id_prodi}', 'KegiatanController@dataTable');
Route::get('/data/kemahasiswaan/json/{id_prodi}', 'KemahasiswaanController@dataTable');
Route::get('/data/akademik/json/{id_prodi}', 'AkademikController@dataTable');
Route::get('/data/kemahasiswaan/json/{id_prodi}?', 'AkreditasiController@dataTable');
Route::get('/data/pegawai/json', 'PegawaiController@dataTable')->name('table.pegawai');
Route::get('/data/pegawai/json/{id_prodi}', 'PegawaiController@dataTable');
Route::get('/data/akademik/json', 'AkademikController@dataTable')->name('table.akademik');
Route::get('/data/jenis_berkas/json', 'TipeberkasController@dataTable')->name('table.jenis_berkas');
Route::get('/data/user_role/json', 'UserRoleController@dataTable')->name('table.user_role');
Route::get('/data/menu_role/json', 'MenuRoleController@dataTable')->name('table.menu_role');
Route::get('/data/role/json', 'RoleSettingController@dataTable')->name('table.role');

Route::post('/ganti_password', 'HomeController@ganti_password')->name('ganti_password');

//recource
Route::resource('akreditasi', 'AkreditasiController');
Route::resource('pegawai', 'PegawaiController');
/*Route::resource('staff/dosen', 'DosenController');*/
Route::resource('akademik', 'AkademikController');
Route::resource('kemahasiswaan', 'KemahasiswaanController');
Route::resource('jenis_berkas', 'TipeberkasController');
Route::resource('kegiatan', 'KegiatanController');
Route::resource('dokumentasi', 'DokumentasiController');
Route::resource('user_role', 'UserRoleController');
Route::resource('menu_role', 'MenuRoleController');
Route::resource('role_setting', 'RoleSettingController');

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/dashboard', 'HomeController@index')->name('home');

Route::get('/cari/pegawai/json', 'PegawaiController@loadData');
Route::get('/cari/user/json', 'UserRoleController@loadData');
Route::get('/prodi/json', 'AkreditasiController@prodi');
Route::get('/filter', 'AkreditasiController@filter');
Route::post('/upload/json', 'BerkasController@upload_berkas')->name('upload.berkas');

Route::get('data/document/{id}', 'BerkasController@getDocument')->name('getDocument');