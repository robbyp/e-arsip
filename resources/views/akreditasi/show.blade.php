<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Prodi</th>
        <th>Gelar Ijazah</th>
        <th>Berkas</th>
        <th>Tahun</th>
        <th>File</th>
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->prodi->nama_prodi }}</td>
        <td>{{ $model->prodi->gelar_ijazah }}</td>
        <td>{{ $model->nama_berkas }}</td>
        <td>{{ $model->tahun }}</td>
        <td><a href="{{asset('storage/public/document/'. $model->file) }}" target="_blank">lihat berkas</a></td>
    </tr>
</table>php 