@extends('layouts.app')
@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Akreditasi Menu
          <a href="{{ route('akreditasi.create') }}" class="btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Tambah Berkas Akreditasi"><i class="icon-plus"></i> Tambah</a>
      </h3>
    </div>
      <div class="form-group">
        <form method="GET"></form>
       <select id="prodi" class="form-contol">
         <option id="op_prodi">--pilih prodi--</option>
          <?php foreach ($prodi as $key): ?>
            <option value="{{ $key->id_prodi }}">{{ $key -> nama_prodi }}</option>
          <?php endforeach ?>
       </select>
       <button class="btn-success" id="filter">Filter</button>
      </div>
</div>
@endsection