@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Akreditasi
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard" class="active"></i> Akreditasi</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Berkas Akreditasi</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="panel-heading">
              
                    <a href="{{ route('akreditasi.create') }}" class="btn btn-success pull-right modal-show btn-create"
                        style="margin-top: -8px;" title="Tambah Berkas Akreditasi"><i class="icon-plus"></i> Tambah</a>
               
            </div>
            <div class="form-group" style="margin-left: 20px;">
                <label>Pilih Prodi</label>
                <select class="form-control" onchange="filter()" id="filter" style="width: 20%">
                    <option value="all">---seluruh program studi---</option>
                    @foreach($prodi as $key)
                    <option value="{{ $key -> id_prodi }}"> {{ $key->nama_prodi }} </option>
                    @endforeach
                </select>
            </div>
            <div class="panel-body">
                <table id="datatable" class="table table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Prodi</th>
                            <th>Nama Berkas</th>
                            <th>Gelar</th>
                            <th>Deskripsi</th>
                            <th>Jenis</th>
                            <th>Berkas</th>
                            <th>#</th>

                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    
    <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection
@push('scripts')
<script>
    function filter() {

        if ($.fn.DataTable.isDataTable('#datatable'))
            $('#datatable').DataTable().destroy();
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('table.akreditasi') }}/" + $('#filter').val(),
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'id'
                },
                {
                    data: 'prodi.nama_prodi',
                    name: 'prodi.nama_prodi'
                },
                {
                    data: 'nama_berkas',
                    name: 'nama_berkas'
                },
                {
                    data: 'prodi.gelar',
                    name: 'prodi.gelar'
                },
                {
                    data: 'deskripsi',
                    name: 'deskripsi'
                },
                {
                    data: 'jenis.tipe',
                    name: 'jenis.tipe'
                },
                {
                    data: 'file',
                    name: 'file'
                },
                {
                    data: 'action',
                    name: 'action',
                    className: "action"
                }
            ]
        });
    }

    $(document).ready(function () {

        if ($('#filter').val() == 'all') {
            $('#datatable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('table.akreditasi') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'id'
                    },
                    {
                        data: 'prodi.nama_prodi',
                        name: 'prodi.nama_prodi'
                    },
                    {
                        data: 'nama_berkas',
                        name: 'nama_berkas'
                    },
                    {
                        data: 'prodi.gelar',
                        name: 'prodi.gelar'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi'
                    },
                    {
                        data: 'jenis.tipe',
                        name: 'jenis.tipe'
                    },
                    {
                        data: 'file',
                        name: 'file'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: "action"
                    }
                ]
            });
        } else {
            filter();
        }

    @if($can_create == false)
        $('.btn-create').hide();
    @endif

    });

    $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
            url: '/cari',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama_prodi,
                            id: item.id_prodi
                        }
                    })
                };
            },
            cache: true
        }
    });

</script>
@endpush
