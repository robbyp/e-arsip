

<form action="{{Route('kemahasiswaan.store')}}" method="POST" name="formName" class="form" enctype="multipart/form-data" id="form_save">
   @csrf

   <div class="form-group">
    <label class="control-label">Nama Berkas</label>
    <input type="text" name="nama_berkas" class="form-control">
  </div>
  <div class="form-group">
    <label class="control-label" >Deskripsi</label>
    <input type="text" name="deskripsi" class="form-control">
  </div>
   <div class="form-group">
    <label class="control-label" >Jenis</label>
    <select class="form-control" name="jenis">
        <option>Pilih Jenis File</option>
      <?php foreach ($tipe as $models): ?>
         <option value="{{$models->id}}" class="form-control">{{ $models->tipe }}</option>
      <?php endforeach ?>
    </select>
  </div>
  <div class="form-group">
    <label class="control-label" >Instansi/Organisasi</label>
    <select class="form-control" name="instansi">
        <option>Pilih Instansi/Organisasi</option>
      <?php foreach ($instansi as $key): ?>
         <option value="{{ $key->id }}" class="form-control">{{ $key->nama_instansi }}</option>
      <?php endforeach ?>
    </select>
  </div>
    <input type="hidden" name="user_id" value="{{ $user }}">
  <div class="form-group">
    <label class="control-label" >Berkas</label>
     <input type="file" name="berkas" class="form-control">
  </div>
  <div class="form-group">
      <button type="submit" class="btn btn-success">Save</button>
  </div>

</form>

<script>
  
    $('#form_save').submit(function (event) {
            event.preventDefault();
            var form = $('#form_save'),
            url = form.attr('action'),
            method =  'POST';
            form.find('.help-block').remove();
            form.find('.form-group').removeClass('has-error');
            var data = new FormData($('#modal-body form')[0]);
            $.LoadingOverlay("show");
            $.ajax({
            url : url,
            method: method,
            /*data : form.serialize(),*/
            data : data,
            contentType: false,
            processData: false,
            success: function (response) {
              $.LoadingOverlay("hide");
                form.trigger('reset');
                $('#modal').modal('hide');
                $('#datatable').DataTable().ajax.reload();
                swal({
                    type : 'success',
                    title : 'Success!',
                    text : 'Data has been saved!'
                })
            },
            error : function (xhr) {
              $.LoadingOverlay("hide");
                    var res = xhr.responseJSON;
                    console.log(res);
                    if ($.isEmptyObject(res) == false) {
                        $.each(res.errors, function (key, value) {
                           swal({
                            type: 'warning',
                            title: 'gagal',
                            text : value 
                           })
                        });
                    }
            }
        })
    });
    </script>








