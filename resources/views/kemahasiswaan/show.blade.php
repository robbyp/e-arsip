<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Nama Berkas</th>
        <th>Oraganisasi</th>
        <th>jenis</th>
        
        <th>File</th>
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->nama_berkas }}</td>
        <td>{{ $model -> instansi -> nama_instansi}}</td>
        <td>{{ $model -> jenis -> tipe}}</td>
        <td><a href="{{asset('storage/public/document/'. $model->file) }}" target="_blank">lihat berkas</a></td>
    </tr>
</table>
