<style type="text/css">
  span.cari {
    z-index:10050;
}
</style>

<form action="{{route('user_role.store')}}" method="POST" id="form_save">
  @csrf
    </div>
      <div class="form-group">
        <label class="control-label">Nama User</label>
          <select class="cari form-control" name="id_user" style="left: 0px;"></select>
    </div>
  <div class="form-group">
    <label class="control-label">Pilih Role</label>
  	<select name="role" class="form-control">
    	<option>---Pilih Role----</option>
    	<?php foreach ($role as $key): ?>
    		<option value="{{$key->id}}">{{ $key->nama_role }}</option>
    	<?php endforeach ?>
    	
    </select>
  </div>
  <div class="form-group">
      <button type="submit" class="btn btn-success">Save</button>
  </div>
</form>

<script type="text/javascript">
  $('.cari').select2({
  dropdownParent: $('#modal')
 });

  $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
        url: '/cari/user/json',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });

  $('#form_save').submit(function (event) {
            event.preventDefault();
            var form = $('#form_save'),
            url = form.attr('action'),
            method =  'POST';
            form.find('.help-block').remove();
            form.find('.form-group').removeClass('has-error');
            var data = new FormData($('#modal-body form')[0]);
            $.ajax({
            url : url,
            method: method,
            /*data : form.serialize(),*/
            data : data,
            contentType: false,
            processData: false,
            success: function (response) {
              
                form.trigger('reset');
                $('#modal').modal('hide');
                $('#datatable').DataTable().ajax.reload();
                swal({
                    type : 'success',
                    title : 'Success!',
                    text : 'Data has been saved!'
                });
            },
            error : function (xhr) {
                var res = xhr.responseJSON;
                if ($.isEmptyObject(res) == false) {
                    $.each(res.errors, function (key, value) {
                        $('#' + key)
                            .closest('.form-group')
                            .addClass('has-error')
                            .append('<span class="help-block"><strong>' + value + '</strong></span>');
                    });
                }
            }
        })
    });
</script>

