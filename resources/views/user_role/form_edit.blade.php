<style type="text/css">
  span.cari {
    z-index:10050;
}
</style>

<form action="{{route('user_role.update', $model->id)}}" method="PUT" id="form_edit">
  @csrf
    
      <div class="form-group">
        <label class="control-label">Nama User</label>
          <select class="cari form-control" name="id_user" style="left: 0px;">
              @php
              use \App\User;
       
              $pegawai = User::select('name')->where('id', $model->user_id)->get();
              
              
           @endphp
           @foreach($pegawai as $value)
              <option value="{{ $model->user_id }}">{{ $value->name }}</option>
           @endforeach
          </select>
    </div>
  <div class="form-group">
    <label class="control-label">Pilih Role</label>
  	<select name="role" class="form-control">
    	<option>---Pilih Role----</option>
    	<?php foreach ($role as $key): ?>
    		<option value="{{$key->id}}">{{ $key->nama_role }}</option>
    	<?php endforeach ?>
    	
    </select>
  </div>
  <div class="form-group">
      <button type="submit" class="btn btn-success">Save</button>
  </div>
</form>

<script>
$('.cari').select2({
  dropdownParent: $('#modal')
 });

  $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
        url: '/cari/user/json',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });
  
$('#form_edit').submit(function (event) {
    event.preventDefault();
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        var form = $('#form_edit'),
        url = form.attr('action'),
        method = 'PUT';

        form.find('.help-block').remove();
        form.find('.form-group').removeClass('has-error');
        var data = form.serialize()

  

    $.ajax({
        url : url,
        method: method,
        /*data : form.serialize(),*/
        data : data,
        success: function (response) {
          
            form.trigger('reset');
            $('#modal').modal('hide');
            $('#datatable').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success!',
                text : 'Data has been saved!'
            });
        },
        error : function (xhr) {
            var res = xhr.responseJSON;
            if ($.isEmptyObject(res) == false) {
                $.each(res.errors, function (key, value) {
                    $('#' + key)
                        .closest('.form-group')
                        .addClass('has-error')
                        .append('<span class="help-block"><strong>' + value + '</strong></span>');
                });
            }
        }
    })
});
</script>