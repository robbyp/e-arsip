@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Setting User Role
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard" class="active"></i> Setting User Role</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar User Role</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="panel-heading">
              
                <a href="{{ route('user_role.create') }}" class="btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Tambah User Role"><i class="icon-plus"></i> Tambah</a>
      
            </div>
            <br>
            <div class="panel-body">
                <table id="datatable" class="table table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Role</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    </table>
                </div>
        </div>
    </div>
    <!-- /.box-body -->
    
    <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection

@push('scripts')
    <script>

        $('#datatable').DataTable({
          responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.user_role') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'user.name', name: 'nama_user'},
                {data: 'role.nama_role', name: 'menu.nama_menu'},
                {data: 'action', name: 'action'}
            ]
        });

         $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
        url: '/cari',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama_prodi,
              id: item.id_prodi
              }
            })
          };
        },
        cache: true
        }
        });

    $('#form_save').submit(function (event) {
            event.preventDefault();
            var form = $('#form_save'),
            url = form.attr('action'),
            method =  'POST';
            form.find('.help-block').remove();
            form.find('.form-group').removeClass('has-error');
            var data = new FormData($('#modal-body form')[0]);
            $.ajax({
            url : url,
            method: method,
            /*data : form.serialize(),*/
            data : data,
            contentType: false,
            processData: false,
            success: function (response) {
              
                form.trigger('reset');
                $('#modal').modal('hide');
                $('#datatable').DataTable().ajax.reload();
                swal({
                    type : 'success',
                    title : 'Success!',
                    text : 'Data has been saved!'
                });
            },
            error : function (xhr) {
                var res = xhr.responseJSON;
                if ($.isEmptyObject(res) == false) {
                    $.each(res.errors, function (key, value) {
                        $('#' + key)
                            .closest('.form-group')
                            .addClass('has-error')
                            .append('<span class="help-block"><strong>' + value + '</strong></span>');
                    });
                }
            }
        })
    });
    </script>
@endpush