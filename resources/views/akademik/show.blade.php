<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Nama Berkas</th>
        <th>Deskripsi</th>
        <th>Prodi</th>
        <th>File</th>
        <th>Aksi</th>
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->nama_berkas }}</td>
        <td>{{ $model->deskripsi }}</td>
        <td>{{ $model->prodi->nama_prodi }}</td>
        <td>{{ $model->file }}</td>

       
         <td><a href="{{asset('storage/public/document/'. $model->file) }}" target="_blank">lihat berkas</a></td>
    </tr>
</table>
