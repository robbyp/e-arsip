@extends('layouts.master')
<script src="adminlte/bower_components/jquery/dist/jquery.min.js"></script>
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard" class="active"></i> Dashboard</a></li>
  </ol>
</section>

<section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-xs-6" id="akademik">
            <!-- small box -->
            <div class="small-box bg-aqua" >
              <div class="inner">
              <h3>{{ $jml_akademik }}</h3>
                <p>Akademik</p>
                
              </div>
              <div class="icon">
                <i class="fa fa-university"></i>
              </div>
              <a href="/akademik" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6" id="kemahasiswaan">
            <!-- small box -->
            <div class="small-box bg-green" >
              <div class="inner">
              <h3>{{ $jml_kemahasiswaan }}</h3>
  
                <p>Kemahasiswaan</p>
              </div>
              <div class="icon">
                <i class="fa fa-group"></i>
              </div>
              <a href="/kemahasiswaan" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
               </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow" id="kegiatan">
              <div class="inner">
              <h3>{{ $jml_kegiatan }}</h3>
  
                <p>Kegiatan</p>
              </div>
              <div class="icon">
                <i class="fa fa-black-tie"></i>
              </div>
              <a href="/kegiatan" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
             </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red" id="akreditasi">
              <div class="inner">
              <h3>{{ $jml_akreditasi }}</h3>
  
                <p>Akreditasi</p>
              </div>
              <div class="icon">
                <i class="fa fa-bookmark-o"></i>
              </div>
              <a href="/akreditasi" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue" id="dokumentasi">
              <div class="inner">
              <h3>{{ $jml_dokumentasi }}</h3>
  
                <p>Dokumentasi</p>
              </div>
              <div class="icon">
                <i class="fa fa-image"></i>
              </div>
              <a href="/dokumentasi" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          <!-- ./col -->
           <!-- ./col -->
           <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange" id="pegawai">
              <div class="inner">
              <h3>{{ $jml_pegawai }}</h3>
                <p>Pegawai</p>
              </div>
              <div class="icon">
                <i class="fa fa-user"></i>
              </div>
              <a href="/pegawai" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
    
</section>
<!-- /.content -->


@endsection


<script>
  $(document).ready(function(){
    $('#kemahasiswaan').hide()
    $('#akademik').hide()
    $('#dokumentasi').hide()
    $('#kegiatan').hide()
    $('#akreditasi').hide()
    $('#pegawai').hide()

    @foreach ($menu as $key)
   @php
       $menu = $key -> menu_id;
   @endphp
     
     @if($menu == 1)
     $('#akademik').show()
     @endif

     @if($menu == 2)
     $('#kemahasiswaan').show()
     @endif

     @if($menu == 3)
     $('#kegiatan').show()
     @endif

     @if($menu == 4)
     $('#dokumentasi').show()
     @endif

     @if($menu == 11)
     $('#akreditasi').show()
     @endif

     @if($menu == 10)
     $('#pegawai').show()
     @endif
   @endforeach
  });
   
  
 </script>
 
