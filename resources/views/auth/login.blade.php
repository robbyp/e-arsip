<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login arsip fst unja</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/assets/img/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" href="adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login_attr/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login_attr/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login_attr/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="login_attr/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login_attr/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login_attr/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="login_attr/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login_attr/css/util.css">
	<link rel="stylesheet" type="text/css" href="login_attr/css/main.css">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form login-form" name="login-form" method="POST" action="{{ route('loginwoy') }}">
                    @csrf    
                    @if(session('pesan'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>{{session('pesan')}}</strong></div>
                        @endif
					<span class="login100-form-title p-b-43">
						Login E-Archive Fakultas Sains dan Teknologi
					</span>
					
					
					<div class="wrap-input100 validate-input" data-validate="username is required">
						<input class="input100" type="text" name="username">
						<span class="focus-input100"></span>
						<span class="label-input100">Username</span>
					</div>
					
					
					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="password">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>


                    <div class="container-login100-form-btn">
                            <button class="login100-form-btn" type="submit">
                                Login
                            </button>
                        </div>
				</form>

				<div class="login100-more" style="background-image: url('login_attr/images/bg-01.jpg');">
				</div>
			</div>
		</div>
	</div>
	
	

	
	
<!--===============================================================================================-->
	<script src="login_attr/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="login_attr/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="login_attr/vendor/bootstrap/js/popper.js"></script>
	<script src="login_attr/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="login_attr/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="login_attr/vendor/daterangepicker/moment.min.js"></script>
	<script src="login_attr/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="login_attr/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="login_attr/js/main.js"></script>

</body>
</html>
