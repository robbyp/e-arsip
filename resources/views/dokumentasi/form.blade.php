<form action="{{Route('dokumentasi.store')}}" method="POST" name="formName" class="form" id="form_save">
   @csrf
   <div class="form-group">
    <label class="control-label">Nama Berkas</label>
    <input type="text" name="nama_berkas" class="form-control">
  </div>
  <div class="form-group">
    <label class="control-label" >Deskripsi</label>
    <input type="text" name="deskripsi" class="form-control">
  </div>
  <div class="form-group">
    <label class="control-label" >Acara</label>
    <input type="text" name="acara" class="form-control">
  </div>
  <div class="form-group">
    <label class="control-label" >Delegasi</label>
    <br><span>*Kosongkan jika tidak ada delegasi</span><br>
    <input type="text" name="delegasi" class="form-control">
  </div>
  <div class="form-group">
    <label class="control-label" >Tahun</label>
    <input type="text" name="tahun" class="form-control">
  </div>
  <div class="form-group">
    <label class="control-label" >Instansi/Organisasi</label>
      <select class="form-control" name="instansi">
          <option >Pilih Instansi/Organisasi</option>
        <?php foreach ($instansi as $key): ?>
          <option value="{{ $key->id }}" class="form-control" class="orw">{{ $key->nama_instansi }}</option>
        <?php endforeach ?>
      </select>
  </div>
  <div class="form-group">
    <label class="control-label" >Jenis</label>
    <select class="form-control" name="jenis" id="jenis">
        <option>Pilih Jenis File</option>
      <?php foreach ($tipe as $models): ?>
         <option value="{{$models->id}}" class="form-control">{{ $models->tipe }}</option>
      <?php endforeach ?>
    </select>
  </div>
    <input type="hidden" name="user_id" value="{{ $user }}">
  <div class="form-group" id="berkas">
    <label class="control-label" >Berkas</label>
    <input type="file" name="berkas" class="form-control">
  </div>
  <div class="form-group" id="berkas_link" style=" display:none">
    <label class="control-label" >Berkas</label>
    <p style="color:blue;">Masukkan Link Google Drive anda</p>
    <input type="text" name="berkas_link" class="form-control" placeholder="link google drive">
  </div>
  <div class="form-group">
      <button type="submit" class="btn btn-success">Save</button>
  </div>
</form>

<script>
    $('#form_save').submit(function (event) {
            event.preventDefault();
            var form = $('#form_save'),
            url = form.attr('action'),
            method =  'POST';
            form.find('.help-block').remove();
            form.find('.form-group').removeClass('has-error');
            var data = new FormData($('#modal-body form')[0]);
            $.LoadingOverlay("show");
            $.ajax({
            url : url,
            method: method,
            /*data : form.serialize(),*/
            data : data,
            contentType: false,
            processData: false,
            success: function (response) {
              $.LoadingOverlay("hide");
                form.trigger('reset');
                $('#modal').modal('hide');
                $('#datatable').DataTable().ajax.reload();
                swal({
                    type : 'success',
                    title : 'Success!',
                    text : 'Data has been saved!'
                });
            },
            error : function (xhr) {
              $.LoadingOverlay("hide");
                var res = xhr.responseJSON;
                if ($.isEmptyObject(res) == false) {
                    $.each(res.errors, function (key, value) {
                      swal({
                            type: 'warning',
                            title: 'gagal',
                            text : value 
                           })
                    });
                }
            }
        })
    });

    $('#jenis').change(function(){
      if($('#jenis').val()=="12"){
        $('#berkas').fadeOut();
        $('#berkas_link').fadeIn();
      }else{
        $('#berkas').fadeIn();
        $('#berkas_link').fadeOut();
      }
    })
    
    $('#tingkat').change(function(){
      if($('#tingkat').val()=="1")
      {
        
      }
    })
    
    </script>








