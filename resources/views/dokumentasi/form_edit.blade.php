<form action="{{Route('dokumentasi.update', $model->id)}}" method="PUT" name="formName" id="form_edit" >
  @csrf
   <div class="form-group">
    <label class="control-label">Nama Berkas</label>
    <input type="text" name="nama_berkas" class="form-control" value="{{$model->nama_berkas}}">
  </div>
  <div class="form-group">
    <label class="control-label" >Deskripsi</label>
    <input type="text" name="deskripsi" class="form-control" value="{{$model->deskripsi}}">
  </div>
  <div class="form-group">
    <label class="control-label" >Acara</label>
    <input type="text" name="acara" class="form-control" value="{{$model->acara}}">
  </div>
  <div class="form-group">
    <label class="control-label" >Delegasi</label>
    <br><span>*Kosongkan jika tidak ada delegasi</span><br>
    <input type="text" name="delegasi" class="form-control" value="{{$model->delegasi}}">
  </div>
  <div class="form-group">
    <label class="control-label" >Tahun</label>
    <input type="text" name="tahun" class="form-control" value="{{$model->tahun}}">
  </div>
  <div class="form-group">
    <label class="control-label" >Instansi/Organisasi</label>
    <select class="form-control" name="instansi">
        <option>Pilih Instansi/Organisasi</option>
      <?php foreach ($instansi as $key): ?>
         <option value="{{ $key->id }}" class="form-control">{{ $key->nama_instansi }}</option>
      <?php endforeach ?>
    </select>
  </div>
  <div class="form-group">
      <label class="control-label" >Jenis</label>
      <select class="form-control" name="jenis">
          <option>Pilih Jenis File</option>
        <?php foreach ($tipe as $models): ?>
           <option value="{{$models->id}}" class="form-control">{{ $models->tipe }}</option>
        <?php endforeach ?>
      </select>
    </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success">Update</button>   
  </div>
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="user_id" value="{{ $user }}">
  
</form>

<script>
  
$('#form_edit').submit(function (event) {
    event.preventDefault();
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        var form = $('#form_edit'),
        url = form.attr('action'),
        method = 'PUT';

        form.find('.help-block').remove();
        form.find('.form-group').removeClass('has-error');
        var data = form.serialize()
        $.LoadingOverlay("show");
  

    $.ajax({
        url : url,
        method: method,
        /*data : form.serialize(),*/
        data : data,
        success: function (response) {
          $.LoadingOverlay("hide");
            form.trigger('reset');
            $('#modal').modal('hide');
            $('#datatable').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success!',
                text : 'Data has been saved!'
            });
        },
        error : function (xhr) {
          $.LoadingOverlay("hide");
            var res = xhr.responseJSON;
            if ($.isEmptyObject(res) == false) {
                $.each(res.errors, function (key, value) {
                  swal({
                            type: 'warning',
                            title: 'gagal',
                            text : value 
                           })
                });
            }
        }
    })
});
</script>