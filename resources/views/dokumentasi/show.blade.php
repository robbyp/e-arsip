


<table class="table table-hover" style="width: 100%;">
    <tr>
                      <th>Id Berkas</th>
                      <th>Nama Berkas</th>
                      <th>Acara</th>
                      <th>Instansi/Organisasi</th>
                      <th>Delegasi</th>
                      <th>Lihat</th>

                     
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->nama_berkas }}</td>
        <td>{{ $model->acara }}</td>
        <td>{{ $model->delegasi }}</td>
        <td>himpunan</td>
        @if($model->link)
        <td><a href="{{ $model->file}}"  target="_blank">lihat digoogle drive</a></td>
        @else
        <td><a href="{{asset('storage/public/document/'. $model->file) }}" target="_blank">lihat berkas</a></td>
        @endif
    </tr>
</table>
