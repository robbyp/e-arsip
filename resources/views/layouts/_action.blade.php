
<a href="{{ $url_show }}" class="btn-show show show_hidden" title="Detail: {{ $model->name }}"><i class="fa fa-eye text-primary" ></i></a>  
<a href="{{ $url_edit }}" class="modal-show edit edit_hidden" title="Edit {{ $model->name }}"><i class="fa fa-pencil text-inverse"></i></a> 
<a href="{{ $url_destroy }}" class="btn-delete hapus show_hidden" title="{{ $model->name }}"><i class="fa fa-trash text-danger"></i></a>