<!DOCTYPE html>
<html>
<style>
    *{
        font-family: "Arial";
    }
</style>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>E-Archive Fakultas Sains dan Teknologi</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!-- Datatables -->
    <link href="{{ asset('assets/vendor/datatables/datatables.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="adminlte/css/AdminLTE.css">
    <!-- autocomplate -->
   <link href="{{ asset('assets/vendor/autoload/select2.min.css') }}" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="adminlte/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="/dashboard" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>E</b>-FST</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>E-ARCHIVE</b>fst</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </a>
                <!-- Sidebar toggle button-->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- Notifications: style can be found in dropdown.less -->
                        <!-- Tasks: style can be found in dropdown.less -->
                        <!-- User Account: style can be found in dropdown.less -->

                        <!-- Control Sidebar Toggle Button -->
                        @php
                         use \App\Menu;
                         use \App\RoleMenu;
                         use \App\UserRole;
                         use \App\Role;
                            

                       
                         $role_user = DB::table('role_user')
                                    ->where('user_id',Auth::user()->id)
                                    ->join('role','role.id','=','role_user.id_role')
                                    ->select('role.nama_role')
                                    ->first();
                        

                        @endphp

                 
                        <li class="nav-item">
                                <div class="dropdown" >
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width:220px; margin-right:3%;">
                                            <i class="fa fa-briefcase"></i> 
                                            @php
                                                if($role_user){
                                                    echo($role_user->nama_role);
                                                }else{
                                                    echo('User biasa');
                                                }
                                            @endphp
                                            
                                           
                                        <span class="caret"></span></button> 
                                        <ul class="dropdown-menu">
                                        <li><a href="#modal_ganti_password" data-toggle="modal"><i class="fa fa-cog"></i> Ganti Password</a></li>
                                        <li><a class="nav-link" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                <i class="fa fa-sign-out"></i>
                                                {{ __('Logout') }}
                                               </a></li>
                                        </ul>
                                </div>    
                            
       
                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                               </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="adminlte/img/avatar5.png" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                      
                        <p>{{ Auth::user()->username }}</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                        <li class="">
                                <a href="/dashboard">
                                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                </a>
                                
                            </li>
                    @php
                    
                    $menu = Menu::whereIn('id', RoleMenu::select('menu_id')->whereIn('id_role', UserRole::select('id_role')->where('user_id', Auth::id()))->get())->get();
                    @endphp
                    @php  $parent = Menu::all()->where('parent', 0)->sortBy('urut'); @endphp
                    @foreach ($parent as $menus)
                      <li class="treeview">
                          <a href="#{{ $menus -> nama_menu }}-child">
                              <i class="fa {{ $menus -> icon }}"></i> <span>{{ $menus->nama_menu }}</span>
                              <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                              </span>
                          </a>
                          <ul class="treeview-menu" id="{{ $menus -> nama_menu }}-child">
                              @php $parent = $menus -> id; @endphp
                              @php $child = $menu->sortBy('urut'); @endphp

                              @foreach ($child as $childs)
                              @if($childs -> parent == $parent)
                                <li>
                                  <a href="{{$childs->url_menu}}"><i class="fa {{$childs->icon}}"> <span>{{ $childs -> nama_menu }}</span></i></a>
                                </li>
                              @endif
                              @endforeach
                          </ul>
                      </li>
                    @endforeach
                </ul>
               

              
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->
        <!-- modal -->
        @include('layouts._modal')
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; Robi Purnomo</a>.</strong>
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

                <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Home tab content -->
                <div class="tab-pane" id="control-sidebar-home-tab">
                    <h3 class="control-sidebar-heading">Recent Activity</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                    <p>Will be 23 on April 24th</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-user bg-yellow"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                    <p>New phone +1(800)555-1234</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                    <p>nora@example.com</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                    <p>Execution time 5 seconds</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                    <h3 class="control-sidebar-heading">Tasks Progress</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Custom Template Design
                                    <span class="label label-danger pull-right">70%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Update Resume
                                    <span class="label label-success pull-right">95%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Laravel Integration
                                    <span class="label label-warning pull-right">50%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Back End Framework
                                    <span class="label label-primary pull-right">68%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                </div>
                <!-- /.tab-pane -->
                <!-- Stats tab content -->
                <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                <!-- /.tab-pane -->
                <!-- Settings tab content -->
                <div class="tab-pane" id="control-sidebar-settings-tab">
                    <form method="post">
                        <h3 class="control-sidebar-heading">General Settings</h3>

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Report panel usage
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Some information about this general settings option
                            </p>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Allow mail redirect
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Other sets of options are available
                            </p>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Expose author name in posts
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Allow the user to show his name in blog posts
                            </p>
                        </div>
                        <!-- /.form-group -->

                        <h3 class="control-sidebar-heading">Chat Settings</h3>

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Show me as online
                                <input type="checkbox" class="pull-right" checked>
                            </label>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Turn off notifications
                                <input type="checkbox" class="pull-right">
                            </label>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Delete chat history
                                <a href="javascript:void(0)" class="text-red pull-right"><i
                                        class="fa fa-trash-o"></i></a>
                            </label>
                        </div>
                        <!-- /.form-group -->
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="adminlte/js/adminlte.min.js"></script>
    {{-- loading --}}
    <script src="adminlte/js/loading.min.js"></script>
    
    <!-- AdminLTE for demo purposes -->
    <script src="adminlte/js/demo.js"></script>
    <!-- autoload -->
    <script src="{{ asset('assets/vendor/autoload/autoload.min.js') }}"></script>
    <!-- Datatables -->
    <script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
     <!-- app.js -->
     <script src="{{ asset('js/app.js') }}"></script>
     <!-- Sweetalert2 -->
    <script src="{{ asset('assets/vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>



    <script>
        $(document).ready(function () {
            $('.sidebar-menu').tree()
        })

    </script>
    @stack('scripts')
</body>

<div class="modal fade" id="modal_ganti_password">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Ganti password</h4>
                </div>
                <div class="modal-body">
                        @if(session('pesan'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>{{session('pesan')}}</strong></div>
                        @endif
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                <form action="{{ route('ganti_password') }}" method="PUT" id="form_ganti_pass">
                    @csrf
                    <div class="form-group">
                        <label for="">Password lama</label>
                        <input type="password" class="form-control cek1" name="pass" placeholder="Password lama">
                        <label for="">Password baru</label>
                        <input type="password" class="form-control cek2" name="pass_baru" placeholder="Password baru">
                        <label for="">Konfirmasi password baru</label>
                        <input type="password" class="form-control cek3" name="con_pass_baru" placeholder="Konfirmasi password baru">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
            </div>
        </div>
    </div>

</html>

<script>
    $('#form_ganti_pass').submit(function(e){
        e.preventDefault();
        if($('.cek1').val() == ""){
            swal({
                    type: 'warning',
                    title: 'kosong',
                    text : 'masukkan password anda!'
                });
        }else{
            if($('.cek2').val() == ""){
            swal({
                    type: 'warning',
                    title: 'kosong',
                    text : 'masukkan password baru anda!'
                });
            }
            else{
                if($('.cek3').val() == ""){
                swal({
                    type: 'warning',
                    title: 'kosong',
                    text : 'masukkan konfirmasi password baru anda!'
                });
                }else{
                    var form = $('#form_ganti_pass');
                    var method = 'post';
                    $.ajax({
                    url : form.attr('action'),
                    method : method,
                    data : form.serialize(),
                    success: function(response){
                    
                    var hasil = JSON.parse(response);
                    if(hasil.hasil == 'gagal'){
                        swal({
                            type : 'warning',
                            title : 'Gagal!',
                            text : hasil.error.password
                        })
                    }else{
                        $('#modal').modal('hide');
                        form.trigger('reset');
                        
                        swal({
                            type : 'success',
                            title : 'Berhasil',
                            text : 'password telah diganti'
                        })
                    }

                    }, 
                    error : function (xhr) {
                    var res = xhr.responseJSON;
                    console.log(res);
                    if ($.isEmptyObject(res) == false) {
                        $.each(res.errors, function (key, value) {
                           swal({
                            type: 'warning',
                            title: 'gagal',
                            text : value 
                           })
                        });
                    }
        }
                    })
                }
                
            }
            

        }

        })
        
        
    
</script>