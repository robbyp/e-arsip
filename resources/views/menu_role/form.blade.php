<form action="{{ route('menu_role.store') }}" method="POST" id="form_save">
   @csrf
  <div class="form-group">
    <label class="label-controller">Pilih Role</label>
  	<select name="id_role" class="form-control">
    	<option>---Pilih Role----</option>
    	<?php foreach ($role as $key): ?>
    		
    		<option value="{{$key->id}}">{{ $key->nama_role }}</option>
    	<?php 

    	endforeach ?>
    	
    </select>
  </div> 
  <div class="form-group">
    <label class="label-controller">Pilih Menu</label>
    <select name="menu_id" class="form-control">
      <option>---Pilih Menu----</option>
      <?php foreach ($menu as $key): ?>
        @if($key->parent != 0)
        <option value="{{$key->id}}">{{ $key->nama_menu }}</option>
        @endif
      <?php 
      endforeach ?>
    </select>
  </div>
  <div class="form-group">
  <label class="label-control">Hak Akses</label>
   <input type="checkbox" name="c" value="1"> Create
   <input type="checkbox" name="u" value="1"> Update
   <input type="checkbox" name="d" value="1"> Delete
  </div>
  </div>
  <div class="form-group">
      <button type="submit" class="btn btn-success">Save</button>
  </div>
</form>

<script>
  
    $('#form_save').submit(function (event) {
            event.preventDefault();
            var form = $('#form_save'),
            url = form.attr('action'),
            method =  'POST';
            form.find('.help-block').remove();
            form.find('.form-group').removeClass('has-error');
            var data = new FormData($('#modal-body form')[0]);
            $.ajax({
            url : url,
            method: method,
            /*data : form.serialize(),*/
            data : data,
            contentType: false,
            processData: false,
            success: function (response) {
              
                form.trigger('reset');
                $('#modal').modal('hide');
                $('#datatable').DataTable().ajax.reload();
                swal({
                    type : 'success',
                    title : 'Success!',
                    text : 'Data has been saved!'
                });
            },
            error : function (xhr) {
                var res = xhr.responseJSON;
                if ($.isEmptyObject(res) == false) {
                    $.each(res.errors, function (key, value) {
                        $('#' + key)
                            .closest('.form-group')
                            .addClass('has-error')
                            .append('<span class="help-block"><strong>' + value + '</strong></span>');
                    });
                }
            }
        })
    });
    </script>
