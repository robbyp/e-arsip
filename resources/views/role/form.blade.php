<form action="{{ route('role_setting.store') }}" method="POST" id="form_create">
   @csrf
  <div class="form-group">
    <label class="label-control">Nama Role</label>
  	<input type="text" name="role" class="form-control">
  </div> 
  
  <div class="form-group">
      <button type="submit" class="btn btn-success">Save</button>
  </div>
</form>

<script>
$('#form_save').submit(function (event) {
  event.preventDefault();
  var form = $('#form_save'),
  url = form.attr('action'),
  method =  'POST';
  form.find('.help-block').remove();
  form.find('.form-group').removeClass('has-error');
  var data = new FormData($('#modal-body form')[0]);
  $.ajax({
  url : url,
  method: method,
  /*data : form.serialize(),*/
  data : data,
  contentType: false,
  processData: false,
  success: function (response) {
    
      form.trigger('reset');
      $('#modal').modal('hide');
      $('#datatable').DataTable().ajax.reload();
      swal({
          type : 'success',
          title : 'Success!',
          text : 'Data has been saved!'
      });
  },
  error : function (xhr) {
      var res = xhr.responseJSON;
      if ($.isEmptyObject(res) == false) {
          $.each(res.errors, function (key, value) {
              $('#' + key)
                  .closest('.form-group')
                  .addClass('has-error')
                  .append('<span class="help-block"><strong>' + value + '</strong></span>');
          });
      }
  }
})
});
</script>
