@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Setting Jenis Berkas
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard" class="active"></i> Setting Jenis Berkas</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Jenis Berkas</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="panel-heading">
              
              <a href="{{ route('jenis_berkas.create') }}" class="btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Tambah Jenis Berkas"><i class="icon-plus"></i> Tambah</a>
      
            </div>
            <br>
            <div class="panel-body">
                <table id="datatable" class="table table-hover" style="width:70%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Berkas</th>
                            <th>Kategori</th>
                            <th>Berkas</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
          </div>
        </div>
    </div>
    <!-- /.box-body -->
    
    <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection


@push('scripts')
    <script>

        $('#datatable').DataTable({
          responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.jenis_berkas') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'tipe', name: 'tipe'},
                {data: 'menu.nama_menu', name: 'menu.nama_menu'},
                {data: 'action', name: 'action'}
            ]
        });

         $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
        url: '/cari',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama_prodi,
              id: item.id_prodi
              }
            })
          };
        },
        cache: true
        }
        });




    </script>
@endpush