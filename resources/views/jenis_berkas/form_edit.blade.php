<form action="{{ route('jenis_berkas.update', $model->id) }}" method="POST" id="form_edit"><div class="form-group">
  <label class="control-label">Nama Jenis</label>
<input type="text" name="nama_jenis" class="form-control" value="{{ $model -> tipe }}">
  
</div>
<div class="form-group">
  <select name="kategori" class="form-control">
    <option>---Pilih Kategori----</option>
    <?php foreach ($menu as $key): ?>
      <?php if($key->parent > 0){ ?>
      <option value="{{$key->id}}">{{ $key->nama_menu }}</option>
    <?php 
      }
    endforeach ?>
    
  </select>
</div>
<div class="form-group">
  <button type="submit" class="btn btn-success">Save</button>
</div>
</form>

<script>
  
$('#form_edit').submit(function (event) {
    event.preventDefault();
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        var form = $('#form_edit'),
        url = form.attr('action'),
        method = 'PUT';

        form.find('.help-block').remove();
        form.find('.form-group').removeClass('has-error');
        var data = form.serialize()

  

    $.ajax({
        url : url,
        method: method,
        /*data : form.serialize(),*/
        data : data,
        success: function (response) {
          
            form.trigger('reset');
            $('#modal').modal('hide');
            $('#datatable').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success!',
                text : 'Data has been saved!'
            });
        },
        error : function (xhr) {
            var res = xhr.responseJSON;
            if ($.isEmptyObject(res) == false) {
                $.each(res.errors, function (key, value) {
                    $('#' + key)
                        .closest('.form-group')
                        .addClass('has-error')
                        .append('<span class="help-block"><strong>' + value + '</strong></span>');
                });
            }
        }
    })
});
</script>