<meta name="_token" content="{{ csrf_token() }}"> 
<style>
.cari_prodi{
    z-index:100000;
}
</style>
{!! Form::model($model, [
    'route' => $model->exists ? ['akreditasi.update', $model->id] : 'akreditasi.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

    @csrf
    <div class="form-group">
        <label for="" class="control-label">Prodi</label>
        <select class="cari_prodi form-control" style="width:500px;" name="id_prodi" id="id_prodi"></select>
        
    </div>

    <div class="form-group">
        <label for="" class="control-label">Nama Berkas</label>
        {!! Form::text('id_berkas', null, ['class' => 'form-control', 'id' => 'id_berkas']) !!}
    </div>
   

{!! Form::close() !!}


