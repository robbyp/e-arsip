<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Prodi</th>
        <th>Gelar Ijazah</th>
        <th>Berkas</th>
        <th>File</th>
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->prodi->nama_prodi }}</td>
        <td>{{ $model->prodi->gelar_ijazah }}</td>
        <td>{{ $model->berkas->nama_berkas }}</td>
        <td><a href="#">lihat berkas</a></td>
    </tr>
</table>
