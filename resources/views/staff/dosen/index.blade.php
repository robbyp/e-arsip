@extends('layouts.app')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Akreditasi Menu
          <a href="{{ route('akreditasi.create') }}" class="btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Tambah Berkas Akreditasi"><i class="icon-plus"></i> Tambah</a>
      </h3>
    </div>
    <div class="panel-body">
          <table id="datatable" class="table table-hover" style="width:100%">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Prodi</th>
                      <th>Gelar</th>
                      <th>Berkas</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                  
              </tbody>
          </table>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.dosen') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'dosen.nama_prodi', name: 'prodi.nama_prodi'},
                {data: 'prodi.gelar_ijazah', name: 'prodi.gelar_ijazah'},
                {data: 'berkas.nama_berkas', name: 'berkas.nama_berkas'},
                {data: 'action', name: 'action'}
            ]
        });

         $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
        url: '/cari',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama_prodi,
              id: item.id_prodi
            }
          })
        };
      },
      cache: true
    }
  });

    </script>
@endpush