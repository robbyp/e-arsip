<script type="text/javascript">
  $('.cari').select2({
  dropdownParent: $('#modal')
 });
  $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
        url: '/cari/pegawai/json',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama_pegawai,
              id: item.id_pegawai
            }
          })
        };
      },
      cache: true
    }
  });
</script>

<style type="text/css">
  span.cari {
    z-index:10050;
}
</style>

<form action="{{Route('pegawai.update', $model->id)}}" method="PUT" name="formName" id="form_edit" >
  @csrf
   <div class="form-group">
    <label class="control-label">Nama Berkas</label>
    <input type="text" name="nama_berkas" class="form-control" value="{{$model->nama_berkas}}">
  </div>
  <div class="form-group">
    <label class="control-label" >Deskripsi</label>
    <input type="text" name="deskripsi" class="form-control" value="{{$model->deskripsi}}">
  </div>
  <div class="form-group">
    <label class="control-label" >Tahun</label>
    <input type="text" name="tahun" class="form-control" value="{{$model->tahun}}">
  </div>
  <div class="form-group">
    <label class="control-label">Pegawai</label>
  <select class="cari form-control" name="pegawai" style="left: 0px;">
    @php
       use \App\Pegawai;

       $pegawai = Pegawai::select('nama_pegawai')->where('id_pegawai', $model->id_pegawai)->get();
       
       
    @endphp
    @foreach($pegawai as $value)
  <option value="{{ $model->id_pegawai }}">{{ $value->nama_pegawai }}</option>
    @endforeach
  </select>
  </div>
   <div class="form-group">
    <label class="control-label" >Jenis</label>
    <select class="form-control" name="jenis">
        <option>Pilih Jenis File</option>
      <?php foreach ($tipe as $models): ?>
         <option value="{{$models->id}}" class="form-control">{{ $models->tipe }}</option>
      <?php endforeach ?>
    </select>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success">Update</button>   
  </div>
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="user_id" value="{{ $user }}">  
</form>

<script>
  
$('#form_edit').submit(function (event) {
    event.preventDefault();
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        var form = $('#form_edit'),
        url = form.attr('action'),
        method = 'PUT';

        form.find('.help-block').remove();
        form.find('.form-group').removeClass('has-error');
        var data = form.serialize()

  

    $.ajax({
        url : url,
        method: method,
        /*data : form.serialize(),*/
        data : data,
        success: function (response) {
          
            form.trigger('reset');
            $('#modal').modal('hide');
            $('#datatable').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success!',
                text : 'Data has been saved!'
            });
        },
        error : function (xhr) {
            var res = xhr.responseJSON;
            if ($.isEmptyObject(res) == false) {
                $.each(res.errors, function (key, value) {
                  swal({
                            type: 'warning',
                            title: 'gagal',
                            text : value 
                           })
                });
            }
        }
    })
});
</script>