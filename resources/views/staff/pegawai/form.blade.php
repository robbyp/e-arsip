<script type="text/javascript">
  $('.cari').select2({
  dropdownParent: $('#modal')
 });

  $('.cari').select2({
        placeholder: 'Cari...',
        ajax: {
        url: '/cari/pegawai/json',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nama_pegawai,
              id: item.id_pegawai
            }
          })
        };
      },
      cache: true
    }
  });
</script>

<style type="text/css">
  span.cari {
    z-index:10050;
}
</style>


<form action="{{Route('pegawai.store')}}" method="POST" enctype="multipart/form-data" id="form_save">
   @csrf
     <input type="hidden" name="id_pegawai" value="{{$user}}">  
     <div class="form-group">
  <div class="form-group">
     <label class="control-label">Nama Berkas</label>
     <input type="text" name="nama_berkas" class="form-control">
  </div>
  <div class="form-group">
     <label class="control-label">Deskripsi</label>
     <input type="text" name="deskripsi" class="form-control">
  </div>
  <div class="form-group">
     <label class="control-label">Tahun</label>
     <input type="text" name="tahun" class="form-control">
  </div>
    <div class="form-group">
    <label class="control-label">Pegawai</label>
    <select class="cari form-control" name="pegawai" style="left: 0px;"></select>
</div>

  <div class="form-group">
    <label class="control-label" >Jenis</label>
    <select class="form-control" name="jenis">
        <option>Pilih Jenis File</option>
         <?php foreach ($jenis as $models): ?>
         <option value="{{$models->id}}" class="form-control">{{ $models->tipe }}</option>
        <?php endforeach ?>
    </select>
  </div>
  <div class="form-group">
    <label class="control-label" >Berkas</label>
    <input type="file" name="berkas" class="form-control">
  </div>
</div>
<input type="hidden" name="user" value="{{$user}}">
<div class="form-group">
  <button type="submit" class="btn btn-success">Save</button>
</div>
</form>


<script>
  
    $('#form_save').submit(function (event) {
            event.preventDefault();
            var form = $('#form_save'),
            url = form.attr('action'),
            method =  'POST';
            form.find('.help-block').remove();
            form.find('.form-group').removeClass('has-error');
            var data = new FormData($('#modal-body form')[0]);
            $.LoadingOverlay("show");
            $.ajax({
            url : url,
            method: method,
            /*data : form.serialize(),*/
            data : data,
            contentType: false,
            processData: false,
            success: function (response) {
              $.LoadingOverlay("hide");
                form.trigger('reset');
                $('#modal').modal('hide');
                $('#datatable').DataTable().ajax.reload();
                swal({
                    type : 'success',
                    title : 'Success!',
                    text : 'Data has been saved!'
                });
            },
            error : function (xhr) {
              $.LoadingOverlay("hide");
                var res = xhr.responseJSON;
                if ($.isEmptyObject(res) == false) {
                    $.each(res.errors, function (key, value) {
                      swal({
                            type: 'warning',
                            title: 'gagal',
                            text : value 
                           })
                    });
                }
            }
        })
    });
    </script>
