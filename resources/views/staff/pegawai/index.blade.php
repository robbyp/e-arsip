@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pegawai
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard" class="active"></i> Pegawai</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Berkas Pegawai</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="panel-heading">
              
                <a href="{{ route('pegawai.create') }}" class="btn-create btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Tambah Berkas Pegawai"><i class="icon-plus"></i> Tambah</a>
      
            </div>
            <div class="form-group" style="margin-left: 20px;">
                <label>Filter</label>
                <select class="form-control" id="filter" style="width: 20%" onchange="filter()">
                      <option value="all">Pegawai dan Dosen</option>
                  @foreach($jenis as $key)
                    <option value="{{ $key -> id }}" > {{ $key->tipe }} </option>
                  @endforeach
                 
                </select>
               
            </div>
            <div class="panel-body">
                <table id="datatable" class="table table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pegawai</th>
                            <th>NIP/NIDN/NIDK</th>
                            <th>Nama Berkas</th>
                            <th>Deskripsi</th>
                            <th>Jenis Berkas</th>
                            <th>Tahun</th>
                            <th>file</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
          </div>
        </div>
    </div>
    <!-- /.box-body -->
    
    <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection

@push('scripts')
    <script>
        

 function filter(){
              
               if($.fn.DataTable.isDataTable('#datatable'))
               $('#datatable').DataTable().destroy();
               $('#datatable').DataTable({
               responsive: true,
              processing: true,
              serverSide: true,

              ajax: {
                    url : "{{ route('table.pegawai') }}/"+$('#filter').val(),
                },
               columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'pegawai.nama_pegawai'},
                {data: 'pegawai.nip'},
                {data: 'nama_berkas'},
                {data: 'deskripsi'},
                {data: 'jenis.tipe'},
                {data: 'tahun'},
                {data: 'file'},
                {data: 'action', name: 'action'}
            ]
                 });
            }
          

          $(document).ready(function(){
            if($('#filter').val()=='all'){
             $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.pegawai') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'pegawai.nama_pegawai'},
                {data: 'pegawai.nip'},
                {data: 'nama_berkas'},
                {data: 'deskripsi'},
                {data: 'jenis.tipe'},
                {data: 'tahun'},
                {data: 'file'},
                {data: 'action', name: 'action'}
            ]
        });
            }else{
              filter();
            }

            @if($can_create == false)
                $('.btn-create').hide();
            @endif
          });
          
         

       

    </script>
@endpush