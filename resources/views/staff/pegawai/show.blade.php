

<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Nama Pegawai</th>
        <th>NIP/NIDN/NIDK</th>
        <th>No SK CPNS</th>
        <th>Berkas</th>
        <th>File</th>
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->pegawai->nama_pegawai }}</td>
        <td>{{ $model->pegawai->nip }}</td>
        <td>{{ $model->pegawai->no_sk_cpns }}</td>
        <td>{{ $model->nama_berkas }}</td>
        <td><a href="{{asset('storage/public/document/'. $model->file) }}" target="_blank">lihat berkas</a></td>
        
    </tr>
</table>
