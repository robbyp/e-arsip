<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staffpegawai extends Model
{
    protected $table = 'staffpegawai';

     protected $fillable = [
    	'id',
    	'id_pegawai',
    	'id_berkas',
    	'created_at',
    	'updated_at'
    ];


     public function pegawai()
    {
    	return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }

    public function berkas()
    {
    	return $this->belongsTo('App\Berkas', 'id_berkas');

    	function jenis_berkas()
    	{
    		return $this->belongsTo('App\JenisBerkas', 'id');
    	}
    }
}
