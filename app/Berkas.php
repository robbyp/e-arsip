<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berkas extends Model
{
     protected $table = 'berkas_tb';

     protected $fillable = ['nama_berkas', 'file'];

      public function jenis(){
        return $this->belongsTo('App\TipeBerkas', 'id_jenis');
    }

      public function prodi(){
        return $this->belongsTo('App\Prodi', 'id_instansi');
    }

    public function instansi(){
        return $this->belongsTo('App\Instansi', 'id_instansi');
    }

    public function kegiatan(){
        return $this->belongsTo('App\Jenis_kegiatan', 'jenis_kegiatan');
    }

    public function pegawai(){
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }
}
