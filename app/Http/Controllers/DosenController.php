<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\StaffDosen;
use App\Prodi;
use DataTables;
class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = StaffDosen::with('pegawai')->get();
        return view('Staff.dosen.index', ['akreditasi' => $model]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $model = new StaffDosen();
         return view('Staff.dosen.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'id_prodi' => 'required|max:10',
            'id_berkas' => 'required|max:10'
        ]);

        $model = StaffDosen::create($request->all());
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $model = StaffDosen::findOrFail($id);
        return view('staff.dosen.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = StaffDosen::findOrFail($id);
        return view('staff.dosen.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'id_prodi' => 'required|max:10',
            'id_berkas' => 'required|max:10'
        ]);
        $model = StaffDosen::findOrFail($id);
        $model->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = StaffDosen::findOrFail($id);
        $model->delete();
    }

     public function loadData(Request $request)
    {
         $cari = $request->q;
            if($cari != null){
                 $data = Prodi::where('nama_prodi', 'like', "%$cari%")->orWhere('id_prodi', 'like', "%$cari%")->get();
                return response()->json($data);
            }
            
            
        
    }

    public function dataTable()
    {
        $model = StaffDosen::with('dosen', 'berkas')->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action', [
                    'model' => $model,
                    'url_show' => route('pegawai.show', $model->id),
                    'url_edit' => route('pegawai.edit', $model->id),
                    'url_destroy' => route('pegawai.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
