<?php

namespace App\Http\Controllers;
use Validator;
use App\Berkas;
use Storage;
use Response;
use Illuminate\Http\Request;

class BerkasController extends Controller
{
    public function upload_berkas(Request $request)
    {

    	 $validator = Validator::make($request->all(), [
        'nama_berkas' => 'required',
        'Berkas' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
     	 ]);

      if ($validator->passes()) {
        $input = $request->all();
        $input['Berkas'] = time().'.'.$request->Berkas->getClientOriginalExtension();
        $request->Berkas->move(public_path('Berkas'), $input['Berkas']);

        Berkas::create($input);
        return response()->json(['success'=>'Berhasil']);
      	}

      return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function getDocument($id){

      $document = Berkas::findOrFail($id);

    $filePath = $document->file_path;

    // file not found
    if( ! Storage::exists($filePath) ) {
      abort(404);
    }

    $pdfContent = Storage::get($filePath);

    // for pdf, it will be 'application/pdf'
    $type       = Storage::mimeType($filePath);
    $fileName   = Storage::name($filePath);

    return Response::make($pdfContent, 200, [
      'Content-Type'        => $type,
      'Content-Disposition' => 'inline; filename="'.$fileName.'"'
    ]);
    }
}
