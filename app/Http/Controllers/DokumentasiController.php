<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Akreditasi;
use App\Prodi;
use App\Fakultas;
use DataTables;
use App\User;
use App\Pegawai;
use App\Berkas;
use App\Akademik;
use App\Instansi;
use Auth;
use App\TipeBerkas;
use Route;
class  DokumentasiController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role_saya = \App\UserRole::where('user_id', Auth::user()->id)->get();
        $menu = \App\Menu::where("nama_rute",Route::currentRouteName())->first();
        $can_update = false;
        $can_delete = false;
        $can_create = false;
        foreach ($role_saya as $key => $value) {
            $menu = \App\RoleMenu::where("id_role", $value->id_role)->where("menu_id", $menu->id)->first();
            if($menu) {
                if($menu->u) $can_update = true;
                if($menu->d) $can_delete = true;
                if($menu->c) $can_create = true;
            }
        }
        return view('dokumentasi.index', ['can_create' => $can_create]);
    }

    public function prodi()
    {
        $prodi = Prodi::all();
        return response()->json($prodi);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Berkas();
           $tipe = TipeBerkas::where('kategori', 4)->get();
          $user = Auth::id();
          $user = Auth::user()->username;
          $instansi = Instansi::all();
            $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
            foreach ($Pegawai as $key) {
                $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();
                $fakultas_ = Fakultas::where('kode_fakultas', $key->unit_kerja)->get();
                foreach ($fakultas as $key) {
                $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
                     return view('dokumentasi.form', ['model'=> $model, 'tipe' => $tipe, 'user' => $user, 'prodi' => $prodi, 'fakultas' => $fakultas_, 'instansi'=>$instansi]);
            } 
            }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
         

           $file = $request->file('berkas'); 

           if(isset($file)){
           $nama_file = time();
           $format = $file->getClientOriginalExtension();
           $store = 'public/document';
           $new_name = $nama_file. '.' .$format;
           $file->storeAs($store, $new_name);     
           
           $this->validate($request, [
            'nama_berkas' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
            'tahun' => 'required',
            'acara' => 'required',
            'jenis_kegiatan' => 'required',
            'delegasi' => 'required',
            'prodi' => 'required',
            'instansi' => 'required',
            'berkas' => 'required|mimes:mpeg,mp4,png,jpeg,jpg,pdf,docx,doc|max:10000040'
             ]);
                    $content = new Berkas;
                    $content -> nama_berkas = $request -> nama_berkas;
                    $content -> deskripsi = $request -> deskripsi;
                    $content -> acara = $request -> acara;
                    $content -> id_jenis = $request -> jenis;
                    $content -> delegasi = $request -> delegasi;
                    $content -> user_id = $request -> user_id;
                    $content -> id_instansi = $request -> instansi;
                    $content -> link = 0;
                    $content -> tahun = $request -> tahun;
                    $content -> file = $new_name;
                    $content -> format = $format;
                    $content -> save();  
            }else{

                $this->validate($request, [
                    'nama_berkas' => 'required',
                    'deskripsi' => 'required',
                    'tahun' => 'required',
                    'tahun' => 'required',
                    'acara' => 'required',
                    'jenis_kegiatan' => 'required',
                    'delegasi' => 'required',
                    'prodi' => 'required',
                    'instansi' => 'required',
                    'berkas_link' => 'required'
                ]);

                    $content = new Berkas;
                    $content -> nama_berkas = $request -> nama_berkas;
                    $content -> deskripsi = $request -> deskripsi;
                    $content -> acara = $request -> acara;
                    $content -> id_jenis = $request -> jenis;
                    $content -> delegasi = $request -> delegasi;
                    $content -> user_id = $request -> user_id;
                    $content -> link = 1;
                    $content -> id_instansi = $request -> instansi;
                    $content -> file = $request-> berkas_link;
                    $content -> tahun = $request -> tahun;
                   
                    $content -> save();  
            }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $model = Berkas::with('instansi', 'jenis')->where('id', $id)->first();
         return view('dokumentasi.show', compact('model'));
    }

    public function form()
    {

        
        return view('dokumentasi.form', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $model = Berkas::findOrFail($id);
        $tipe = TipeBerkas::where('kategori', 4)->get();
        $user = Auth::id();
        $user = Auth::user()->username;
        $instansi = Instansi::all();
        $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
         foreach ($Pegawai as $key) {
             $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();
             $fakultas_ = Fakultas::where('kode_fakultas', $key->unit_kerja)->get();
             foreach ($fakultas as $key) {
             $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
                  return view('dokumentasi.form_edit', ['model'=> $model, 'tipe' => $tipe, 'user' => $user, 'prodi' => $prodi, 'fakultas' => $fakultas_, 'instansi'=>$instansi]);
             } 
         }
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_berkas' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
           
            'acara' => 'required',
            'jenis' => 'required',
            'delegasi' => 'required',
         
            'instansi' => 'required',
            
        ]);
        $content = new Berkas;
                    $content -> nama_berkas = $request -> nama_berkas;
                    $content -> deskripsi = $request -> deskripsi;
                    $content -> acara = $request -> acara;
                    $content -> id_jenis = $request -> jenis;
                    $content -> delegasi = $request -> delegasi;
                    $content -> user_id = $request -> user_id;
                    
                    $content -> id_instansi = $request -> instansi;
                   
                    $content -> tahun = $request -> tahun;
                   
                    $content -> save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Berkas::findOrFail($id);
        $model->delete();
    }

     public function modal()
    {
        $user = Auth::user()->username;
        $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
        foreach ($Pegawai as $key) {
            $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();

            foreach ($fakultas as $key) {
               $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
            }
        }
        return view('akademik.form', ['prodi' => $prodi]);
    }

    public function dataTable()
    {
          $model = Berkas::with(['jenis', 'instansi'])->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori', 4)->get())->get(); 
          return DataTables::of($model)
            ->addColumn('action', function ($model) {
                $role_saya = \App\UserRole::where('user_id', Auth::user()->id)->get();
                $menu = \App\Menu::where("datatables", Route::currentRouteName())->first();
                $can_update = false;
                $can_delete = false;
                $can_create = false;
                foreach ($role_saya as $key => $value) {
                    $menu = \App\RoleMenu::where("id_role", $value->id_role)->where("menu_id", $menu->id)->first();
                    if($menu) {
                        if($menu->u) $can_update = true;
                        if($menu->d) $can_delete = true;
                        if($menu->c) $can_create = true;
                    }
                }
             
                $can_show = true;

                if($can_show){
                    $show = '<a href="dokumentasi/'.$model->id.'" class="btn-show" title="hapus"><i class="fa fa-eye text-success"></i></a>'; 
                }
                if( $can_delete == true){
                    $delete = '<a href="dokumentasi/'.$model->id.'" class="btn-delete hapus show_hidden" title="hapus"><i class="fa fa-trash text-danger"></i></a>';
                }else{
                    $delete = "";
                }

                if( $can_update == true){
                    $update ='<a href="dokumentasi/'.$model->id.'/edit" class="modal-show edit edit_hidden" title="Edit"><i class="fa fa-pencil text-inverse"></i></a>';
                }else{
                    $update = "";
                }
                $html ="<div>".$show." ".$update." ".$delete."</div>";
                return $html;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
