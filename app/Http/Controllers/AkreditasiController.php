<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Akreditasi;
use App\Prodi;
use App\Fakultas;
use DataTables;
use App\Berkas;
use App\User;
use App\Pegawai;
use App\TipeBerkas;
use Auth;
use Route;
class AkreditasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->username;
        $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
        foreach ($Pegawai as $key) {
            $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();
            foreach ($fakultas as $key) {
               $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
            }
        }
        $role_saya = \App\UserRole::where('user_id', Auth::user()->id)->get();
        $menu = \App\Menu::where("nama_rute",Route::currentRouteName())->first();
        $can_update = false;
        $can_delete = false;
        $can_create = false;
        foreach ($role_saya as $key => $value) {
            $menu = \App\RoleMenu::where("id_role", $value->id_role)->where("menu_id", $menu->id)->first();
            if($menu) {
                if($menu->u) $can_update = true;
                if($menu->d) $can_delete = true;
                if($menu->c) $can_create = true;
            }
        }

        return view('akreditasi.index', ['prodi' => $prodi, 'can_create' => $can_create, 'can_update' => $can_update, 'can_delete' => $can_delete]);
    }

    public function prodi(){


        $prodi = Prodi::all();
        return response()->json($prodi);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = 'create';
        $user = Auth::user()->username;
        $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
        foreach ($Pegawai as $key) {
            $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();
            foreach ($fakultas as $key) {
               $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
            }
        }
          $model = new Berkas();
          $instansi = Prodi::all();
          $tipe = TipeBerkas::where('kategori', 11)->get();
          $user = Auth::id();
         return view('akreditasi.form', ['form'=> $form, 'model'=> $model, 'tipe' => $tipe, 'user' => $user, 'instansi' => $instansi, 'prodi' => $prodi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_berkas' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
            'jenis' => 'required',
            'jenis_kegiatan' => 'required',
            'delegasi' => 'required',
            'prodi' => 'required',
            'berkas' => 'required|mimes:mpeg,mp4,png,jpeg,jpg,pdf,docx,doc|max:10000040'
        ]);

            $file = $request->file('berkas'); 
            $nama_file = time();
            $format = $file->getClientOriginalExtension();
            $store = 'public/document';
            $new_name = $nama_file. '.' .$format;
            $file->storeAs($store, $new_name);  

            $content = new Berkas;
            $content -> nama_berkas = $request -> nama_berkas;
            $content -> deskripsi = $request -> deskripsi;
            $content -> id_instansi = $request -> prodi;
            $content -> id_jenis = $request -> jenis;
            $content -> user_id = $request -> user_id;
            $content -> tahun = $request -> tahun;
            $content -> file = $new_name;
            $content -> format = $format;
            $content -> save();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $model = Berkas::findOrFail($id);
        return view('akreditasi.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $model = Berkas::findOrFail($id);
         $tipe = TipeBerkas::where('kategori', 11)->get();
          $user = Auth::id();
          $user = Auth::user()->username;
          $form = 'edit';
            $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
            foreach ($Pegawai as $key) {
                $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();
                $fakultas_ = Fakultas::where('kode_fakultas', $key->unit_kerja)->get();
                foreach ($fakultas as $key) {
                $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
                     return view('akreditasi.form_edit', ['form'=>$form, 'model'=> $model, 'tipe' => $tipe, 'user' => $user, 'prodi' => $prodi, 'fakultas' => $fakultas_]);
            } 
            }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_berkas' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
            'jenis' => 'required',
            'jenis_kegiatan' => 'required',
            'delegasi' => 'required',
            'prodi' => 'required',
            
        ]);

        $content = Berkas::findOrFail($id);
        $content -> nama_berkas = $request -> nama_berkas;
        $content -> deskripsi = $request -> deskripsi;
        $content -> id_instansi = $request -> id_instansi;
        $content -> id_jenis = $request -> id_jenis;
        $content -> user_id = $request -> user_id;
        $content -> tahun = $request -> tahun;
        $content -> save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Berkas::findOrFail($id);
        $model->delete();
    }

     public function modal()
    {
         $user = Auth::user()->username;
        $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
        foreach ($Pegawai as $key) {
            $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();

            foreach ($fakultas as $key) {
               $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
            }
        }
        return view('akreditasi.form', ['prodi' => $prodi]);
    }

    public function dataTable($id=null)
    {
        if($id == null || $id == 'all' ){
            $model = Berkas::with('jenis', 'prodi')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori', 11)->get())->get();
        }else{
            $model = Berkas::with('jenis', 'prodi')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori', 11)->get())->where('id_instansi', $id)->get();
        }
            return DataTables::of($model)
            ->addColumn('action', function ($model) {
                $role_saya = \App\UserRole::where('user_id', Auth::user()->id)->get();
                $menu = \App\Menu::where("datatables", Route::currentRouteName())->first();
                $can_update = false;
                $can_delete = false;
                $can_create = false;
                foreach ($role_saya as $key => $value) {
                    $menu = \App\RoleMenu::where("id_role", $value->id_role)->where("menu_id", $menu->id)->first();
                    if($menu) {
                        if($menu->u) $can_update = true;
                        if($menu->d) $can_delete = true;
                        if($menu->c) $can_create = true;
                    }
                }
                
                $can_show = true;

                if($can_show){
                    $show = '<a href="akreditasi/'.$model->id.'" class="btn-show" title="hapus"><i class="fa fa-eye text-success"></i></a>'; 
                }
                if( $can_delete == true){
                    $delete = '<a href="akreditasi/'.$model->id.'" class="btn-delete hapus show_hidden" title="hapus"><i class="fa fa-trash text-danger"></i></a>';
                }else{
                    $delete = "";
                }

                if( $can_update == true){
                    $update ='<a href="akreditasi/'.$model->id.'/edit" class="modal-show edit edit_hidden" title="Edit"><i class="fa fa-pencil text-inverse"></i></a>';
                }else{
                    $update = "";
                }
                $html ="<div>".$show." ".$update." ".$delete."</div>";
                return $html;

            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
    
}