<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Akreditasi;
use App\Prodi;
use App\Fakultas;
use App\TipeBerkas;
use DataTables;
use App\User;
use App\Pegawai;
use App\Berkas;
use App\Akademik;
use App\Menu;
use Auth;
class TipeberkasController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       
        return view('jenis_berkas.index');
    }

    public function prodi()
    {
        $prodi = Prodi::all();
        return response()->json($prodi);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $model = new TipeBerkas();
          $tipe = TipeBerkas::all();
          $user = Auth::id();
          $menu = Menu::all();
         return view('jenis_berkas.form', ['model'=> $model, 'tipe' => $tipe, 'user' => $user, 'menu' => $menu]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $content = new TipeBerkas;
        $content -> tipe = $request -> nama_jenis;
        $content -> Kategori = $request -> kategori;
        $content -> save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $model = TipeBerkas::findOrFail($id);
        return view('jenis_berkas.show', compact('model'));
    }

    public function form()
    {

        
        return view('jenis_berkas.form', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
          $model = TipeBerkas::findOrFail($id);
          $tipe = TipeBerkas::all();
          $user = Auth::id();
          $menu = Menu::all();
          return view('jenis_berkas.form_edit', compact('model'), ['tipe' => $tipe, 'user' => $user, 'menu' => $menu]);
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $content = TipeBerkas::findOrFail($id);
        $content -> tipe = $request -> nama_jenis;
        $content -> Kategori = $request -> kategori;
        $content -> save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = TipeBerkas::findOrFail($id);
        $model->delete();
    }

     public function modal()
    {
        $user = Auth::user()->username;
        $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
        foreach ($Pegawai as $key) {
            $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();

            foreach ($fakultas as $key) {
               $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
            }
        }
        return view('jenis_berkas.form', ['prodi' => $prodi]);
    }

    public function dataTable()
    {
        
        $model = TipeBerkas::with('menu')->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action', [
                    'model' => $model,
                    'url_show' => route('jenis_berkas.show', $model->id),
                    'url_edit' => route('jenis_berkas.edit', $model->id),
                    'url_destroy' => route('jenis_berkas.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}