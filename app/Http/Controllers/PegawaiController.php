<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Staffpegawai;
use App\Prodi;
use App\Berkas;
use App\Pegawai_berkas;
use DataTables;
use Auth;
use DB;
use App\TipeBerkas;
use Route;
class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $jenis = TipeBerkas::where('kategori', 10)->get();
        $model = Staffpegawai::with('pegawai')->get();
        $role_saya = \App\UserRole::where('user_id', Auth::user()->id)->get();
        $menu = \App\Menu::where("nama_rute",Route::currentRouteName())->first();
        $can_update = false;
        $can_delete = false;
        $can_create = false;
        foreach ($role_saya as $key => $value) {
            $menu = \App\RoleMenu::where("id_role", $value->id_role)->where("menu_id", $menu->id)->first();
            if($menu) {
                if($menu->u) $can_update = true;
                if($menu->d) $can_delete = true;
                if($menu->c) $can_create = true;
            }
        }
        return view('staff.pegawai.index', ['pegawai' => $model, 'jenis' => $jenis, 'can_create' => $can_create]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = DB::table('pegawai_berkas')->join('berkas_tb', 'berkas_tb.id', 'pegawai_berkas.id_berkas')->get();
        $user = Auth::id();
        $tipe = TipeBerkas::where('kategori', 10)->get();
  


         return view('staff.pegawai.form', ['user' => $user, 'jenis' => $tipe, 'model'=>$model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_berkas' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
            'jenis' => 'required',
            'pegawai' => 'required',
            
            'berkas' => 'mimes:mpeg,mp4,png,jpeg,jpg,pdf,docx,doc|max:10000040'
        ]);

           $file = $request->file('berkas'); 
           $nama_file = time();
           $format = $file->getClientOriginalExtension();
           $store = 'public/document';
           $new_name = $nama_file. '.' .$format;
           $file->storeAs($store, $new_name); 
        $content = new Berkas;
        $content -> nama_berkas = $request -> nama_berkas;
        $content -> deskripsi = $request -> deskripsi;
        $content -> id_jenis = $request -> jenis;
        $content -> tahun = $request -> tahun;
        $content -> id_pegawai = $request -> pegawai;
        $content -> user_id = $request -> user;
        $content -> file = $new_name;
        $content -> format = $format;
        $content -> save();
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $model = Berkas::with('pegawai', 'jenis')->where('id', $id)->first();
        return view('staff.pegawai.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $model = Berkas::findOrFail($id);
        $user = Auth::id();
        $tipe = TipeBerkas::where('kategori', 10)->get();
        return view('staff.pegawai.form_edit', ['user' => $user, 'tipe' => $tipe, 'model'=>$model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_berkas' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
            'jenis' => 'required',
          
            'pegawai' => 'required',
           
           
        ]);
        $content = Berkas::findOrFail($id);
        $content -> nama_berkas = $request -> nama_berkas;
        $content -> deskripsi = $request -> deskripsi;
        $content -> id_jenis = $request -> jenis;
        $content -> tahun = $request -> tahun;
        $content -> id_pegawai = $request -> pegawai;
        $content -> user_id = $request -> user_id;
        $content -> save();
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Berkas::findOrFail($id);
        $model->delete();
    }

     public function loadData(Request $request)
    {
         $cari = $request->q;
            if($cari != null){
                 $data = Pegawai::where('nama_pegawai', 'like', "%$cari%")->orWhere('id_pegawai', 'like', "%$cari%")->get();
                return response()->json($data);
            }  
    }

    public function dataTable($id=null)
    {
        if($id == null || $id == 'all' ){
            $model = Berkas::with('pegawai', 'jenis')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori', 10)->get())->get();
        }else{
            $model = Berkas::with('jenis', 'pegawai')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori', 10)->get())->where('id_jenis', $id)->get();
        }

        /*return User::find(1)->works()->where('user_works.active','=','1')->get();*/
       /* $model = Berkas::with('jenis_berkas')->whereIn('jenis', TipeBerkas::select('id')->where('kategori', 1)->get())->get();*/
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                $role_saya = \App\UserRole::where('user_id', Auth::user()->id)->get();
                $menu = \App\Menu::where("datatables", Route::currentRouteName())->first();
                $can_update = false;
                $can_delete = false;
                $can_create = false;
                foreach ($role_saya as $key => $value) {
                    $menu = \App\RoleMenu::where("id_role", $value->id_role)->where("menu_id", $menu->id)->first();
                    if($menu) {
                        if($menu->u) $can_update = true;
                        if($menu->d) $can_delete = true;
                        if($menu->c) $can_create = true;
                    }
                }
                
                $can_show = true;

                if($can_show){
                    $show = '<a href="pegawai/'.$model->id.'" class="btn-show" title="hapus"><i class="fa fa-eye text-success"></i></a>'; 
                }
                if( $can_delete == true){
                    $delete = '<a href="pegawai/'.$model->id.'" class="btn-delete hapus show_hidden" title="hapus"><i class="fa fa-trash text-danger"></i></a>';
                }else{
                    $delete = "";
                }

                if( $can_update == true){
                    $update ='<a href="pegawai/'.$model->id.'/edit" class="modal-show edit edit_hidden" title="Edit"><i class="fa fa-pencil text-inverse"></i></a>';
                }else{
                    $update = "";
                }
                $html ="<div>".$show." ".$update." ".$delete."</div>";
                return $html;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
