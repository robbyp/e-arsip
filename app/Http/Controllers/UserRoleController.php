<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Akreditasi;
use App\Prodi;
use App\Fakultas;
use App\TipeBerkas;
use DataTables;
use App\User;
use App\Pegawai;
use App\Berkas;
use App\Akademik;
use App\Menu;
use Auth;
use App\UserRole;
use App\Role;
class UserRoleController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('user_role.index');
    }

    public function prodi()
    {
        $prodi = Prodi::all();
        return response()->json($prodi);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $model = new TipeBerkas();
          $tipe = TipeBerkas::all();
          $user = Auth::id();
          $menu = Menu::all();
          $role = Role::all();
         return view('user_role.form', ['model'=> $model, 'tipe' => $tipe, 'user' => $user, 'menu' => $menu, 'role' => $role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $content = new UserRole;
        $content -> user_id = $request -> id_user;
        $content -> id_role = $request -> role;
        $content -> save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

        $model = UserRole::findOrFail($id);
        return view('user_role.show', compact('model'));
    }

    public function form()
    {

        
        return view('jenis_berkas.form', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $model = UserRole::find($id);
        $tipe = TipeBerkas::all();
        $user = Auth::id();
        $menu = Menu::all();
        $role = Role::all();
          return view('user_role.form_edit', compact('model'), ['tipe' => $tipe, 'user' => $user, 'menu' => $menu, 'role'=>$role]);
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $content = UserRole::findOrFail($id);
        $content -> user_id = $request -> id_user;
        $content -> id_role = $request -> role;
        $content -> save();
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = UserRole::findOrFail($id);
        $model->delete();
    }

    public function loadData(Request $request)
    {
         $cari = $request->q;
            if($cari != null){
                 $data = User::where('name', 'like', "%$cari%")->orWhere('username', 'like', "%$cari%")->get();
                return response()->json($data);
            }  
    }

     public function modal()
    {
        $user = Auth::user()->username;
        $Pegawai = Pegawai::select('unit_kerja')->where('nip', $user)->get();
        foreach ($Pegawai as $key) {
            $fakultas = Fakultas::select('id_fakultas')->where('kode_fakultas', $key->unit_kerja)->get();

            foreach ($fakultas as $key) {
               $prodi = Prodi::where('id_fakultas', $key->id_fakultas)->get();
            }
        }
        return view('user_role.form', ['prodi' => $prodi]);
    }

    public function dataTable()
    {
        
        $model = UserRole::with('Role', 'User')->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action', [
                    'model' => $model,
                    'url_show' => route('user_role.show', $model->id),
                    'url_edit' => route('user_role.edit', $model->id),
                    'url_destroy' => route('user_role.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}