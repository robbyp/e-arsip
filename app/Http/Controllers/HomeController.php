<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Akreditasi;
use App\Prodi;
use App\Fakultas;
use App\TipeBerkas;
use DataTables;
use App\User;
use App\Pegawai;
use App\Berkas;
use App\Akademik;
use App\Menu;
use Auth;
use App\UserRole;
use App\RoleMenu;
use App\Role;
use Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $menu_akademik = 1;
        $jml_akademik = Berkas::select('id')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori',  $menu_akademik)->get())->count();
        $menu_kemahasiswaan = 2;
        $jml_kemahasiswaan = Berkas::select('id')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori',  $menu_kemahasiswaan)->get())->count();
        $menu_kegiatan = 3;
        $jml_kegiatan = Berkas::select('id')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori',  $menu_kegiatan)->get())->count();
        $menu_akreditasi = 11;
        $jml_akreditasi = Berkas::select('id')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori',  $menu_akreditasi)->get())->count();
        $menu_pegawai = 10;
        $jml_pegawai = Berkas::select('id')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori',  $menu_pegawai)->get())->count();
        $menu_dokumentasi = 4;
        $jml_dokumentasi = Berkas::select('id')->whereIn('id_jenis', TipeBerkas::select('id')->where('kategori',  $menu_dokumentasi)->get())->count();
        
        $id_user = Auth::user()->id;
        $role = UserRole::select('id_role')->where('user_id', $id_user)->first();
        $menu = RoleMenu::whereIn('id_role', $role)->get();
        

        return view('home', ['menu' => $menu, 'jml_akademik' => $jml_akademik, 'jml_kemahasiswaan' => $jml_kemahasiswaan, 'jml_kegiatan' => $jml_kegiatan, 'jml_akreditasi'=>$jml_akreditasi, 'jml_dokumentasi'=>$jml_dokumentasi, 'jml_pegawai'=>$jml_pegawai]);
    }

    public function ganti_password(Request $request){
        
       $password = Auth::user()->password;
       
        // if (!(Hash::check($request->get('pass'), $password))) {
        //     // The passwords matches
        //     // return back()->with("pesan","password lama anda salah!");
        //     $data['error']['password'] = 'password lama salah!';
            
            
           
        // }

        if($request -> pass_baru != $request -> con_pass_baru){ 
            $data['error']['password'] = 'konfirmasi password baru anda salah!';
        }

         if(strcmp($request->get('pass'), $request->get('pass_baru')) == 0){
             //Current password and new password are same
             $data['error']['password'] = 'password baru anda sama dengan password lama!';
             
         }

        $validatedData = $request->validate([
            'pass' => 'required',
            'pass_baru' => 'required|string|min:6',
        ]);
        
        
         //Change Password
         $id = Auth::user()->id;
         $user = User::findOrFail($id);
         $user->password = bcrypt($request->get('pass_baru'));
         $user->save();

        if (empty($data['error'])) {
            $data['hasil'] = 'sukses';
        }else{
            $data['hasil'] = 'gagal';
        }
        echo json_encode($data);
        

        

      
        
        
        

    }
}
