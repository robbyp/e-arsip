<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akreditasi extends Model
{
     protected $table = 'akreditasi';

     protected $fillable = [
    	'id',
    	'id_prodi',
    	'id_berkas',
    	'created_at',
    	'updated_at'
    ];


     public function prodi()
    {
    	return $this->belongsTo('App\Prodi', 'id_prodi');
    }

    public function berkas()
    {
    	return $this->belongsTo('App\Berkas', 'id_berkas');
    }
}
