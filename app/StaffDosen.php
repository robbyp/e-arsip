<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffDosen extends Model
{
    protected $table = 'staffdosen';

      public function dosen()
    {
    	return $this->belongsTo('App\Dosen', 'id_dosen');
    }

    public function berkas()
    {
    	return $this->belongsTo('App\Berkas', 'id_berkas');

    	function jenis_berkas()
    	{
    		return $this->belongsTo('App\JenisBerkas', 'id');
    	}
    }
}
