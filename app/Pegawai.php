<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';

   protected $primaryKey = 'id_pegawai';
     protected $fillable = [
    	'id_pegawai',
    	'nama_pegawai',
    	'created_at',
    	'updated_at'
    ];
}
