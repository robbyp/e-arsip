<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
     protected $table = 'menu_role';

      public function Role()
    {
    	return $this->belongsTo('App\Role', 'id_role');
    }
     public function Menu()
    {
    	return $this->belongsTo('App\Menu', 'menu_id');
    }
}
