<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
     protected $table = 'role_user';
     
      public function Role()
    {
    	return $this->belongsTo('App\Role', 'id_role');
    }
     public function User()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function RoleMenu()
    {
    	return $this->hasMany('App\RoleMenu', 'id_role');
    }
    
}
