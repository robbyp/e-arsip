<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akademik extends Model
{
    protected $table = 'akademik';

    protected $primaryKey = 'id_akademik';
    protected $fillable = [
    	'id_prodi',
    	'nama_akademik',
    	'id_berkas',
    	'updated_at'
    ];
     public function Berkas(){
        return $this->belongsTo('App\Berkas', 'id_berkas', 'id');
    }
}
