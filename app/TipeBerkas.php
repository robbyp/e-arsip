<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeBerkas extends Model
{
    protected $table = 'tipe_berkas';

    public function menu()
    {
    	return $this->belongsTo('App\Menu', 'kategori');
    } 

}
