<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
     protected $table = 'prodi';
     protected $primaryKey = 'id_prodi';
     protected $fillable = [
    	'id_prodi',
    	'nama_prodi',
    	'created_at',
    	'updated_at'
    ];
     public function get_kategori(){
        return $this->belongsTo('App\Akreditasi', 'id_prodi', 'id');
    }
}
